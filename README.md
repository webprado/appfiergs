### **Projeto FIERGS**
### Sistema de controle de Produtos e Categorias

Este documento tem como objetivo a implantação do sistema.

Obs.: Neste projeto foi adotado a exclusão dos registros de banco de dados de forma lógica, o que significa; existe uma coluna **excluido** tanto na tabela **categorias** como **produtos** onde é do tipo **INT** onde recebe os valores "0" (zero) para não excluído ou "1" (um) para excluído. 
Esta técnica foi adotada pensando na possibilidade de recuperação de dados excluídos.

### Repositório do Sistema
```sh
$ git clone https://gitlab.com/webprado/appfiergs.git
```
### Documentação
Dentre os arquivos e pasta do sistema, existe a pasta **Docs**, onde nesta encontra-se os seguintes arquivos:

#### *Banco de dados*
Neste está o backup do banco de dados
```sh
$ Docs/AppFIERGSdb.sql
``` 

No projeto, no arquivo .env, alterar o valor das chaves **DB_HOST**, **DB_PORT** (caso haja necessidade, porta padrão 3306), **DB_DATABASE**, **DB_USERNAME** e **DB_PASSWORD**

#### *Acessos*
Neste estão os dados de acesso dos usuários cadastrados no sistema
```sh
$ Docs/Dados_Acesso.txt
``` 
#### *Manual do Sistema*
Manual de uso do sistema com informações da restrições por perfil de acesso
```sh
$ Docs/Manual_Sistema.pdf
``` 
