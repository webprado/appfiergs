<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use App\Models\Categoria;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class CategoriasController extends Controller
{
    /**
     * Exibir uma listagem do recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categorias = DB::table('categorias AS C')
                        ->Select(['C.*']);

        if($request->has('_token'))
        {
            if($request->filled('ativo'))
                $categorias->where('C.ativo','=', $request->ativo);

            if($request->filled('nomedescricao'))
            {
                $categorias->where('C.nome','LIKE', "%{$request->nomedescricao}%");
                $categorias->Where('C.descricao','LIKE', "%{$request->nomedescricao}%");
            }
        }

        $categorias->where('C.excluido','=', 0);
        $categorias->orderBy('C.nome', 'ASC');
        $dadosLista = $categorias->get();

        return View('categorias.listar', compact('dadosLista'));
    }

    /**
     * Mostrar o formulário para criar um novo recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function novo()
    {
        if(auth()->user()->can('vendedor'))
        {
            return redirect('categorias')
                    ->with('alerta', 'Usuário sem permissão para criar categoria');
        }

        return View('categorias.novo');
    }

    /**
     * Armazene um recurso recém-criado no armazenamento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function salvar(Request $request)
    {
        if($request->has('_token'))
        {
            Categoria::create($request->all());

            return redirect('categorias')
                    ->with('status', 'Categoria '.$request->nome.' cadastrada com sucesso!');
        }
    }

    /**
     * Mostra o formulário para editar o recurso especificado.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $categoria = Categoria::find($id);
        return View('categorias.editar')
                ->with('categoria', $categoria);
    }

    /**
     * Atualize o recurso especificado no armazenamento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function atualizar(Request $request, $id)
    {
        $categoria = Categoria::find($id);
        $categoria->nome = $request->nome;
        $categoria->descricao = $request->descricao;
        $categoria->ativo = $request->ativo;
        $categoria->save();

        return redirect('categorias')
                ->with('status', 'Categoria '.$categoria->nome.' atualizada com sucesso!');
    }

    /**
     * Remova o recurso especificado do armazenamento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $categoria = Categoria::find($id);
        $categoria->excluido = 1;
        $categoria->ativo = 0;
        $categoria->save();

        return redirect('categorias')
                ->with('status', 'Categoria '.$categoria->nome.' excluída com sucesso!');
    }
}
