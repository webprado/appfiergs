<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use App\Models\Produto;
use Illuminate\Support\Facades\DB;

class ProdutosController extends Controller
{
    /**
     * Exibir uma listagem do recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $produtos = DB::table('produtos AS P')
                        ->Select(
                            [
                                'P.*',
                                'C.nome AS categoria',
                            ]
                        )
                        ->join('categorias AS C', 'P.id_categoria', '=', 'C.id');

        if($request->has('_token'))
        {
            if($request->filled('ativo'))
                $produtos->where('P.ativo','=', $request->ativo);

            if($request->filled('nomedescricao'))
            {
                $produtos->where('P.nome','LIKE', "%{$request->nomedescricao}%");
                $produtos->Where('P.descricao','LIKE', "%{$request->nomedescricao}%");
            }

            if($request->filled('categoria'))
                $produtos->where('P.id_categoria','=', $request->categoria);
        }

        $produtos->where('P.excluido','=', 0);
        $produtos->where('C.excluido','=', 0);
        $produtos->where('C.ativo','=', 1);
        $produtos->orderBy('P.nome', 'ASC');
        $dadosLista = [];
        $dadosLista['Produtos'] = $produtos->get();

        $categorias = DB::table('categorias AS C')
                        ->select(['C.*'])
                        ->where('C.ativo', '=', 1)
                        ->where('C.excluido', '=', 0)
                        ->orderBy('C.nome', 'ASC')
                        ->get();

        $dadosLista['Categorias'] = $categorias;

        return View('produtos.listar', compact('dadosLista'));
    }

    /**
     * Mostrar o formulário para criar um novo recurso.
     *
     * @return \Illuminate\Http\Response
     */
    public function novo()
    {
        if(auth()->user()->can('vendedor'))
        {
            return redirect('produtos')
                    ->with('alerta', 'Usuário sem permissão para criar produto');
        }

        $categorias = DB::table('categorias AS C')
                        ->Select(['C.*'])
                        ->where('C.ativo','=', 1)
                        ->where('C.excluido','=', 0)
                        ->orderBy('C.nome', 'ASC')
                        ->get();

        return View('produtos.novo')
                ->with('categorias', $categorias);
    }

    /**
     * Armazene um recurso recém-criado no armazenamento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function salvar(Request $request)
    {
        if($request->has('_token'))
        {
            Produto::create($request->all());

            return redirect('produtos')
                    ->with('status', 'Produto '.$request->nome.' cadastrado com sucesso!');
        }
    }

    /**
     * Mostra o formulário para editar o recurso especificado.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {
        $dadosEdicao = [];
        $dadosEdicao['Produto'] = Produto::find($id);

        $categorias = DB::table('categorias AS C')
                        ->Select(['C.*'])
                        ->where('C.ativo','=', 1)
                        ->where('C.excluido','=', 0)
                        ->orderBy('C.nome', 'ASC')
                        ->get();

        $dadosEdicao['Categorias'] = $categorias;

        return View('produtos.editar')
                ->with('dadosEdicao', $dadosEdicao);
    }

    /**
     * Atualize o recurso especificado no armazenamento.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function atualizar(Request $request, $id)
    {
        $produto = Produto::find($id);
        $produto->nome = $request->nome;
        $produto->descricao = $request->descricao;
        $produto->ativo = $request->ativo;
        $produto->save();

        return redirect('produtos')
                ->with('status', 'Produto '.$produto->nome.' atualizado com sucesso!');
    }

    /**
     * Remova o recurso especificado do armazenamento.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $produto = Produto::find($id);
        $produto->excluido = 1;
        $produto->ativo = 0;
        $produto->save();

        return redirect('produtos')
                ->with('status', 'Produto '.$produto->nome.' excluído com sucesso!');
    }
}
