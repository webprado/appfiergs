<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Categoria>
 */
class CategoriaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
                [
                'nome' => 'Eletrodoméstico',
                'descricao' => 'Aqui é a descrição dos eletrodomésticos',
                ]
                ,
                // [
                // 'nome' => 'Casa',
                // 'descricao' => 'Aqui é a descrição de coisas para casa.',
                // ]
        ];
    }
}
