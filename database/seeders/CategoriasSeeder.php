<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Categoria;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categorias = [
            [
                'nome' => 'Eletrodoméstico',
                'descricao' => 'Aqui é a descrição dos eletrodomésticos',
            ],
            [
                'nome' => 'Casa',
                'descricao' => 'Aqui é a descrição de coisas para casa.',
            ],
            [
                'nome' => 'Informática',
                'descricao' => 'Aqui é a descrição de equipamentos de informática.',
            ]
        ];

        foreach ($categorias as $key => $value) {
            Categoria::create($value);
        }
    }
}
