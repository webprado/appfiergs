<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use \Illuminate\Database\Eloquent\Factory;
use DB;

use \App\Models\Categoria;
use \App\Models\Produto;

class ProdutosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoria = DB::table('categorias')->inRandomOrder()->limit(1)->get();

        $produtos = [
            [
                'nome' => 'Microondas',
                'descricao' => 'Descrição de como é o microondas',
                'Preco' => 330,
                'id_categoria' => $categoria[0]->id
            ],
            [
                'nome' => 'Nodebook Dell',
                'descricao' => 'Notebook é uma ótima ferramenta de trabalho. Muito bom equipamento.',
                'Preco' => 1879.79,
                'id_categoria' => $categoria[0]->id
            ]
        ];

        foreach ($produtos as $key => $value) {
            Produto::create($value);
        }
    }
}
