<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleAndPermissionSeeder extends Seeder
{
    public function run()
    {
        Permission::create(['name' => 'criar-produtos']);
        Permission::create(['name' => 'listar-produtos']);
        Permission::create(['name' => 'atualizar-produtos']);
        Permission::create(['name' => 'excluir-produtos']);

        Permission::create(['name' => 'criar-categorias']);
        Permission::create(['name' => 'listar-categorias']);
        Permission::create(['name' => 'atualizar-categorias']);
        Permission::create(['name' => 'excluir-categorias']);

        $gerenteRole = Role::create(['name' => 'gerente']);
        $vendedorRole = Role::create(['name' => 'vendedor']);

        $gerenteRole->givePermissionTo([
            'criar-produtos',
            'listar-produtos',
            'atualizar-produtos',
            'excluir-produtos',
            'criar-categorias',
            'listar-categorias',
            'atualizar-categorias',
            'excluir-categorias',
        ]);

        $vendedorRole->givePermissionTo([
            'listar-produtos',
            'listar-categorias',
        ]);
    }
}
