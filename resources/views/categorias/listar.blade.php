<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Categorias') }}
        </h2>
        <h3>
            {{ __('Listagem') }}
        </h3>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                        Filtros
                    </h2>
                    <form>
                        @csrf
                        <div>
                            <x-input-label for="nomedescricao" :value="__('Nome / Descrição')" />
                            <div class="col-sm-4">
                                <x-text-input id="nomedescricao" name="nomedescricao" type="text" class="form-control" value="{{request()->nomedescricao}}"/>
                            </div>
                        </div>

                        <div class="campos10x">
                            <x-input-label for="ativo" :value="__('Ativo')" />
                            <div class="col-sm-2">
                                <select class="form-control" id="ativo" name="ativo">
                                    <option value="" {{is_null(request()->ativo) ? 'selected' : ''}} >--Todos--</option>
                                    <option value="1" {{request()->ativo==1 ? 'selected' : ''}} >Sim</option>
                                    <option value="0" {{!is_null(request()->ativo) && request()->ativo==0 ? 'selected' : ''}} >Não</option>
                                </select>
                            </div>
                        </div>
                        <div class="campos10x">
                            <x-primary-button>{{ __('Pesquisar') }}</x-primary-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">

                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if (session('alerta'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ session('alerta') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @can('gerente')
                        <div style="padding-top: 0px; padding-bottom: 4px;">
                            <a href="{{ route('categorias/novo') }}" class="btn btn-success" value="login">
                                {{ __('Novo') }}
                            </a>
                        </div>
                    @endcan

                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Ativo</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($dadosLista)==0)
                                <tr><td colspan="6">Não encontrado</td></tr>
                            @else
                                @foreach ($dadosLista as $categoria )
                                    <tr>
                                        <td>{{$categoria->id}}</td>
                                        <td>{{$categoria->nome}}</td>
                                        <td>{{$categoria->descricao}}</td>
                                        <td>{{ $categoria->ativo==1 ? 'Sim' : 'Não' }}</td>
                                        <td>
                                            <a class="btn btn-secondary btn-sm" href="{{ route('categorias/editar', ['id'=>$categoria->id]) }}" title="Visualizar/Editar">
                                                <i class="fa-solid fa-pen-to-square"></i>
                                            </a>
                                            @can('gerente')
                                                &nbsp;&nbsp;
                                                <a class="btn btn-danger btn-sm" href="javascript:excluir({{$categoria->id}})" title="Exluir">
                                                    <i class="fa-regular fa-trash-can"></i>
                                                </a>
                                            @endcan
                                        </td>

                                    </tr>
                                @endforeach
                            @endif
                        <tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        function excluir(id){
            if(confirm('Deseja excluir a categoria?')==true){
                location.href = 'categorias/excluir/' + id;
            }
        }
    </script>
</x-app-layout>
