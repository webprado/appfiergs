<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Produtos') }}
        </h2>
        <h3>
            {{ __('Novo') }}
        </h3>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <form method="POST" action="{{ route('produtos/salvar') }}">
                        @csrf
                        <div>
                            <x-input-label for="nome" :value="__('Nome')" />
                            <div class="col-sm-6">
                                <x-text-input id="nome" name="nome" type="text" class="form-control"/>
                            </div>
                        </div>

                        <div class="campos10x">
                            <x-input-label for="descricao" :value="__('Descrição')" />
                            <div class="col-sm-6">
                                <x-text-input id="descricao" name="descricao" type="text" class="form-control" rows="5"/>
                            </div>
                        </div>

                        <div class="campos10x">
                            <x-input-label for="preco" :value="__('Preço')" />
                            <div class="col-sm-2">
                                <x-text-input id="preco" name="preco" type="text" class="form-control"/>
                            </div>
                        </div>

                        <div class="campos10x">
                            <x-input-label for="id_categoria" :value="__('Categoria')" />
                            <div class="col-sm-2">
                                <select class="form-control" id="id_categoria" name="id_categoria">
                                    <option value="">--Selecione--</option>
                                    @foreach ($categorias as $categoria )
                                        <option value="{{$categoria->id}}">{{$categoria->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="campos10x">
                            <x-input-label for="ativo" :value="__('Ativo')" />
                            <div class="col-sm-2">
                                <select class="form-control" id="ativo" name="ativo">
                                    <option value="1">Sim</option>
                                    <option value="0">Não</option>
                                </select>
                            </div>
                        </div>

                        <div style="padding-top: 10px;">
                            <x-primary-button>{{ __('Salvar') }}</x-primary-button>
                            <a href="{{ route('produtos') }}" class="btn btn-danger" style="padding: 0.20rem 0.75rem" >
                                {{ __('Cancelar') }}
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
