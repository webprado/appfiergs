<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CategoriasController;
use App\Http\Controllers\ProdutosController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    // Categorias
    Route::get('/categorias', [CategoriasController::class, 'index'])->name('categorias');
    Route::get('/categorias/novo', [CategoriasController::class, 'novo'])->name('categorias/novo');
    Route::post('/categorias/salvar', [CategoriasController::class, 'salvar'])->name('categorias/salvar');
    Route::get('/categorias/editar/{id}', [CategoriasController::class, 'editar'])->name('categorias/editar');
    Route::post('/categorias/atualizar/{id}', [CategoriasController::class, 'atualizar'])->name('categorias/atualizar');
    Route::get('/categorias/excluir/{id}', [CategoriasController::class, 'excluir'])->name('categorias/excluir');

    // Produtos
    Route::get('/produtos', [ProdutosController::class, 'index'])->name('produtos');
    Route::get('/produtos/novo', [ProdutosController::class, 'novo'])->name('produtos/novo');
    Route::post('/produtos/salvar', [ProdutosController::class, 'salvar'])->name('produtos/salvar');
    Route::get('/produtos/editar/{id}', [ProdutosController::class, 'editar'])->name('produtos/editar');
    Route::post('/produtos/atualizar/{id}', [ProdutosController::class, 'atualizar'])->name('produtos/atualizar');
    Route::get('/produtos/excluir/{id}', [ProdutosController::class, 'excluir'])->name('produtos/excluir');
});



require __DIR__.'/auth.php';
